+++
title = "Digital Ocean Guide"
date = "2017-05-27T20:13:12-05:00"
draft = false
banner = "img/blog/hugo-site-gen.png"
type = "writing_work_item"
tech1 = "Progressive Web App"
tech1_content = "The debt calculator is a progressive web app, meaing it is a normal website that is optimized for mobile devices. It is treated like a normal app on Android and iOS."
+++

After a trip to Montreal with some friends, we saw the need for a way to keep track of who owes each other how much. This web app automatically calculates how much everyone in a group owes to each other.