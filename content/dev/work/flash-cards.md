+++
title = "Flash Cards"
date = "2017-05-27T20:13:12-05:00"
draft = false
banner = "img/work/flash-cards.png"
link = "https://bitbucket.org/shaqb4/flashcards"
weight = 4
type = "dev_work_item"
tech1 = "Java"
tech1_content = "I used Java due to its cross-platform nature and straightforward GUI development."
tech2 = "JavaFX"
tech2_content = "JavaFX is a GUI framework that comes with Java out of the box. Working with it extremely enjoyable, especially with the use of Scene Builder, a drag and drop GUI builder."
tech3 = "Eclipse IDE"
tech3_content = "Eclipse is my primary Java development environment, along with the Gradle build system."
+++

I built this flash card software to help me study for my Spanish exams - instead of actually studying. It reads in files in a `data` folder, each file containing tab-separated questions and answers with one pair per line. In this case, I put spanish vocabulary and their english translation on each card. The program supports multiple "decks" of flash cards simply by creating multiple files in the `data` directory, providing a dropdown to choose which deck to use. In addition, you can cycle through the cards in order with the "Previous" and "Next" buttons, or select random cards. To flip the card over, just click on the flash card.