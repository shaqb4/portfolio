+++
title = "Best Minecraft Servers"
date = "2017-05-27T20:13:12-05:00"
draft = false
banner = "img/work/bmcs.png"
link = "http://bestmcservers.com"
weight = 2
type = "dev_work_item"
tech1 = "Laravel"
tech1_content = "The backend is written in Laravel, a powerful PHP framework."
tech2 = "Bootstrap CSS"
tech2_content = "The User Interface was designed and created with Twitter's Bootstrap CSS framework, allowing for a responsive and mobile friendly experience."
tech3 = "Hosting"
tech3_content = "A complete Digital Ocean production and test environment was created to host the webiste, requiring the installation of a firewall and required dependencies."
+++

BestMCServers is a website allowing minecraft players to to list game servers that they run. Servers are ranked based on user votes, allowing players to easily find good servers to play on. In addition, websockets are used to ping the servers directly and find information, such as the number of players currently online and which plugins are installed.