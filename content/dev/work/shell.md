+++
title = "Command Shell"
date = "2017-05-27T20:13:12-05:00"
draft = false
banner = "img/work/shell.png"
link = "https://bitbucket.org/shaqb4/command-shell"
weight = 5
type = "dev_work_item"
tech1 = "C"
tech1_content = "The low level and control that C provides makes it a good choice for interfacing directly with the operating system. I used memory management, multiple processes and executed other programs."
tech2 = "Unix"
tech2_content = "The operating system used in most of my software courses was Unix. I gained experience using useful tools and learned how to the operating system works. Some examples are `awk`, `grep` and piping commands together."
tech3 = "Multiple Processes"
tech3_content = "I had to learn about how processes work in Unix and how to properly create and end them with C scripts."
+++

This simple command line shell was an assignment for my Systems Programming course at the University of Miami. It reads in commands and then executes the requested action or program. All special characters, such as quotes, are marked with a parity bit and processed with a certain order of precedence (e.g. single quotes before double quotes). It supports shell variables and aliases, as well as an echo command. Any other command is treated as an existing executable and called in its own child process with `fork` and `execvp`. 