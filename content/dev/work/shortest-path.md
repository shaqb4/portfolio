+++
title = "Shortest Path Map"
date = "2017-05-27T20:13:12-05:00"
draft = false
banner = "img/work/shortest-path.jpg"
type = "dev_work_item"
link = "https://bitbucket.org/shaqb4/shortest-path"
weight = 3
tech1 = "C++"
tech1_content = "I used C++ with object-oriented principles to create both the console interface and the graphical map window."
tech2 = "Version Control"
tech2_content = "The code base was tracked with the Git version control software throughout the development software. This made it possible to work on one feature at a time without having to worry about messing up existing functionality."
tech3 = "Dijkstra's Algorithm"
tech3_content = "As part of the course, I learned about Dijkstra's efficient algorithm for traversing a graph and finding the shortest path between two nodes. Therefore, I implemented my own priority queue data structure to use for the algorithm."
+++

I developed a shortest path program for my Algorithms course at the University of Miami. It creates a graph structure by reading location and road data from files, then prompts the user to enter two cities in the United States. It then calculates the shortest path between the cities and shows it on a map that includes elevation.