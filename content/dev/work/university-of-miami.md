+++
title = "University of Miami Website"
date = "2017-05-27T20:13:12-05:00"
draft = false
banner = "img/work/umiami.png"
link = "http://welcome.miami.edu/"
weight = 1
type = "dev_work_item"
tech1 = "PHP"
tech1_content = "Much of the server-side functionality is written in PHP to create dynamic content and interface with other services."
tech2 = "Google Custom Search API"
tech2_content = "I created a PHP code-base to interact with an instance of this API. It includes both simple and advanced search queries as well as pagination."
tech3 = "Cascade CMS"
tech3_content = "I created PHP scripts that generate reports and automate tasks relating to sites built with the Cascade Content Management System."
+++

I've been a student web developer at the University of Miami since my freshman year. I do backend development with PHP for their main website and create tools for internal use. Some of the features I've worked on include the cross-site search functionality for the many of the University's websites, as well as creating scripts to copy archived content so it can be moved to a new location. I have also created automated scripts to generate reports for internal use. All of this is done with object-oriented and SOLID principles so that the code I write is flexible and readable. During my time working at the University, I have learned a lot about working with a team, documenting everything I do and following standards.