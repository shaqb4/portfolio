+++

date = "2017-06-14T02:24:08-04:00"

title = "Thank you"
keywords = ["web design", "shahkedb", "web development", "SEO", "writer", "web content", "contact"]
+++

### Thank you, your message has been sent! I will get back to you soon. 
#### Meanwhile, feel free to look around some more.

###### thank-you-gap
