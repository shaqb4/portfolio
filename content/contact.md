+++
title = "Contact"
id = "contact"
keywords = ["web design", "shahkedb", "web development", "SEO", "writer", "web content", "contact"]
+++

# Get in touch

If you are interested in my services or have any questions for me, please get in touch. I'd be happy to discuss a project you have in mind, my prices,my process, or even just a friendly note.

I can be reached directly through my email, found on this page under the "Address" section, or through the form below. I check my email several times a day, so I'll make sure to get back to you quickly!