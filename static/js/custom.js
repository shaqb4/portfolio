
    var Page = (function() {

    // expose the view model through the Page module
    return {
        hideOfflineWarning: function() {
            // enable the live data
            //document.querySelector(".arrivals-list").classList.remove('loading')
            // remove the offline message
            //document.getElementById("offline").remove();
            // load the live data
        },
        showOfflineWarning: function() {
            // disable the live data
            //document.querySelector(".arrivals-list").classList.add('loading')
                // load html template informing the user they are offline
            var request = new XMLHttpRequest();
            request.open('GET', './offline.html', true);

            request.onload = function() {
                if (request.status === 200) {
                    // success
                    // create offline element with HTML loaded from offline.html template
                    /*var offlineMessageElement = document.createElement("div");
                    offlineMessageElement.setAttribute("id", "offline");
                    offlineMessageElement.innerHTML = request.responseText;
                    document.getElementById("main").appendChild(offlineMessageElement);*/
                } else {
                    // error retrieving file
                    console.warn('Error retrieving offline.html');
                }
            };

            request.onerror = function() {
                // network errors
                console.error('Connection error');
            };

            request.send();
        }
    }

});


function initSW() {
// Register the service worker if available.
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./sw.js').then(function(reg) {
        console.log('Successfully registered service worker', reg);
    }).catch(function(err) {
        console.warn('Error whilst registering service worker', err);
    });
}
else {
    console.log('Failed to register service worker.');
}

window.addEventListener('online', function(e) {
    // Resync data with server.
    console.log("You are online");
    //Page.hideOfflineWarning();
    //Arrivals.loadData();
}, false);

window.addEventListener('offline', function(e) {
    // Queue up events for server.
    console.log("You are offline");
    Page.showOfflineWarning();
}, false);

// Check if the user is connected.
if (navigator.onLine) {
    //Arrivals.loadData();
} else {
    // Show offline message
    Page.showOfflineWarning();
}
}

//window.onload = function () {initSW();}